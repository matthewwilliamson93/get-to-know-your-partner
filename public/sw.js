const CACHE_NAME = "random-questions-cache";
const URLS_TO_CACHE = ["/",
                     "/index.html",
                     "styles.css",
                     "questions.js"
];

self.addEventListener("install", event =>
    event.waitUntil(caches.open(CACHE_NAME)
        .then(cache => cache.addAll(URLS_TO_CACHE))
    )
);

self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request).then(response => {
            if (response) {
                return response;
            }

            const fetchRequest = event.request.clone();
            return fetch(fetchRequest).then(response => {
                if (!response || response.status !== 200 || response.type !== "basic") {
                    return response;
                }

                const responseToCache = response.clone();
                caches.open(CACHE_NAME).then(cache =>
                    cache.put(event.request, responseToCache)
                );

                return response;
            });
        })
    );
});
