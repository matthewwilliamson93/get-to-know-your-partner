const paragraph = "questionParagraph";
const answer = "questionsAnswered";

function getAnswers() {
    const answered_str = window.localStorage.getItem(answer);
    let answered = [];
    if (answered_str !== null) {
        answered = answered_str.split(",").map((index) => parseInt(index));
    }

    return answered;
}

function putAnswers(answered) {
    const answered_str = answered.map((index) => index.toString()).join(",");
    window.localStorage.setItem(answer, answered_str);
}

function markRead() {
    const answered = getAnswers();

    const entry = document.getElementById(paragraph);
    answered.push(entry.questionNum);
    answered.sort();

    putAnswers(answered);
}

function randomChoice(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

function setButtonDisplay(show) {
    const showDisplay = (show) => show ? "unset" : "none";
    document.getElementById("discussedButton").style.display = showDisplay(show);
    document.getElementById("skipButton").style.display = showDisplay(show);
    document.getElementById("resetButton").style.display = showDisplay(!show);
}

function randomQuestion() {
    const entry = document.getElementById(paragraph);
    entry.style.opacity = 0;
    setTimeout(() => {
        const questionIndex = Array.from(Array(questions.length).keys());
        const answered = getAnswers();
        const openQuestions = questionIndex.filter((q) => !answered.includes(q));
        if (openQuestions.length === 0) {
            entry.innerText = "Yayy you finished!\n... So are you compatible?";
            entry.style.opacity = 1;
            setButtonDisplay(false);
            return;
        }

        const index = randomChoice(openQuestions);
        entry.innerText = `Question #${index + 1}: ${questions[index]}`;
        entry.questionNum = index;
        entry.style.opacity = 1;
        setButtonDisplay(true);
    }, 1000);
}

function resetQuestions() {
    setTimeout(() => {
        window.localStorage.removeItem(answer);
        randomQuestion();
    }, 1000);
}

const questions = [
    "How many kids do you want?",
    "What values do you want to install in your children?",
    "How do you want to discipline your kids?",
    "What would you do if one of your children said he was homosexual?",
    "What if our children didn't want to go to college?",
    "How much say do children have in a family?",
    "How comfortable are you around children?",
    "Would you be opposed to having our parents watch the children so we can spend time alone together?",
    "Would you put your children in private or public school?",
    "What are your thoughts on home schooling?",
    "Would you be willing to adopt if we couldn't have kids?",
    "Would you be willing to seek medical treatment if we couldn't have kids naturally?",
    "Do you believe it's OK to discipline your child in public?",
    "How do you feel about paying for your kid's college education?",
    "How far apart do you want kids?",
    "Would you want someone to stay home with the kids or use day care?",
    "How would you feel if our kids wanted to join the military rather than go to college?",
    "How involved do you want grandparents to be in our parenting?",
    "How will we handle parental decisions?",
    "Would you be willing to go to marriage counseling if we were having marital problems?",
    "If there is a disagreement between me and your family, whose side do you choose?",
    "How do you handle disagreements?",
    "Would you ever consider divorce?",
    "Would you rather discuss issues as they arise or wait until you have a few problems?",
    "How would you communicate you aren't satisfied sexually?",
    "What is the best way to handle disagreements in a marriage?",
    "How can I be better at communicating with you?",
    "What are your views on infidelity?",
    "What are your religious views on marriage?",
    "What's more important, work or family?",
    "What are your political views?",
    "What are your views on birth control?",
    "Would you rather be rich and miserable or poor and happy?",
    "Who will make the biggest decisions of the household?",
    "What would you do if someone said something bad about me?",
    "Would you follow the advice of your family before your spouse?",
    "What do you believe the role of a wife is?",
    "Who should do household chores?",
    "What do you believe the role of a husband is?",
    "Do you enjoy traveling?",
    "How often would you like to travel?",
    "Where would you like to travel?",
    "How important is spending time alone to you?",
    "How would you feel about me going on a trip with the girls (boys) for a couple of weeks?",
    "How important is spending time with friends to you?",
    "What would be the perfect weekday evening to you?",
    "What would we do if we both had a break from work, but each of us had different ideas on how to spend it?",
    "How often would you want to visit your family?",
    "How often will your family visit us?",
    "How often would you want my family to visit?",
    "How often would you want to visit my family?",
    "Do you have a family history of diseases or genetic abnormalities?",
    "What if one of your family members said he disliked me?",
    "How would you handle holiday family visits?",
    "If your parents became ill, would you take them in?",
    "If my parents became ill, would you mind taking them in?",
    "Does anyone in your family suffer from alcoholism?",
    "What is your medical family history?",
    "Would you be opposed to mental health treatment?",
    "If I had to change my diet because of medical concerns, would you be willing to change yours?",
    "Are you willing to exercise with me to improve our health?",
    "Where do you want to live?",
    "Would you mind moving if I had to relocate with my job?",
    "What would you do if we fell out of love?",
    "What are your career aspirations?",
    "What would you like to be doing five or ten years from now?",
    "What do you think is the best way to keep the love alive in a marriage?",
    "How do you think life will change if we got married?",
    "What is the best thing about marriage?",
    "What is the worst thing about marriage?",
    "What is your idea of the best weekend?",
    "How important are wedding anniversaries to you?",
    "How would you like to spend special days?",
    "What kind of grandparent do you want to be someday?",
    "What type of house do you want to live in?",
    "What is your biggest fear about marriage?",
    "What excites you about getting married?",
    "What do wedding rings mean to you?",
    "Are you afraid to talk to me about anything?",
    "What do you think would improve our relationship?",
    "What would be one thing you would change about our relationship?",
    "Do you have any doubts about the future of our relationship?",
    "Do you believe love can pull you through anything?",
    "Is there anything you don't trust about me?",
    "Which would you choose - dishes or laundry?",
    "Do you like pets?",
    "How many pets do you want?",
    "What to do you want to do during retirement?",
    "At what age would you like to retire?"
];
